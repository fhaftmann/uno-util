# README #

### What is this repository for? ###

This library provides a robust and pythonic abstraction
layer over LibreOffice's UNO API, consisting of a python3 library
and command line utilities.
