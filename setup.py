
import uno_util

from distutils.core import setup

version = '0.13'

description = uno_util.__doc__.strip()
long_description = '''This library provides a robust and pythonic abstraction
layer over LibreOffice's UNO API, consisting of a python3 library
and command line utilities.
'''


settings = dict(

    name = 'uno-util',
    version = version,

    packages = ['uno_util'],
    scripts = ['bin/oconvert', 'bin/ocrypt', 'bin/oprint'],

    author = 'Florian Haftmann',
    author_email = 'florian.haftmann@informatik.tu-muenchen.de',
    description = description,
    long_description = long_description,

    classifiers = [
      'Development Status :: 1 - Planning',
      'Intended Audience :: Developers',
      'License :: OSI Approved :: GNU General Public License v2 (GPLv2)',
      'Operating System :: OS Independent',
      'Programming Language :: Python :: 3',
      'Topic :: Utilities'
    ],
    url = 'http://bitbucket.org/haftmann/python3-uno-util'

)

setup(**settings)
