
'''Fundamental office access.'''

# Author: Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
# Licensed under the GNU General Public License v2 (GPLv2)


from contextlib import contextmanager
from os import path
import random
import tempfile
import subprocess
import time
import sys

import uno
from com.sun.star.connection import NoConnectException


__all__ = ['fresh_context', 'existing_context', 'batch_run', 'interactive_run']


office_name = 'soffice.bin'
office_executable = path.join(sys.path[-1], office_name)
  ## make use of an implementation detail in uno to find out office installation


def office_args(args, user_settings = None):

    return [office_executable] + \
      (['-env:UserInstallation=file://' + user_settings] if user_settings is not None else []) + \
      args


def create_pipe_name():

    random.seed()
    return 'uno' + str(random.random())[2:]


def end_process(proc, retry, timeout_interval):

    proc.terminate()
    for k in range(retry):
        if proc.poll() is not None:
            return proc.poll()
        time.sleep(timeout_interval)
    proc.kill()
    return None


def obtain_context(connection_txt, retry, timeout_interval):

    local_ctxt = uno.getComponentContext()
    resolver = local_ctxt.ServiceManager.createInstanceWithContext('com.sun.star.bridge.UnoUrlResolver',
      local_ctxt)
    for remaining in reversed(range(retry)):
        try:
            ctxt = resolver.resolve('uno:' + connection_txt + 'StarOffice.ComponentContext')
            return ctxt
        except NoConnectException:
            if remaining > 0:
                time.sleep(timeout_interval)
            else:
                raise


@contextmanager
def context(user_settings, args, retry = 10, retry_first_startup = 2, timeout_interval = 0.3):

    pipe_name = create_pipe_name()
    connection_txt = 'pipe,name={};urp;'.format(pipe_name)
    base_call = office_args(args, user_settings)

    procs = []
    for attempt_process in range(retry):
        proc = subprocess.Popen(base_call + ['--accept=' + connection_txt])
        procs.append(proc)
        try:
            ctxt = obtain_context(connection_txt, retry if attempt_process > 0 else retry_first_startup,
              timeout_interval)
            break
        except NoConnectException:
            ctxt = None
    if ctxt is None:
        raise  ## reraise connnection exception on obtainment failure
    try:
        yield ctxt
    finally:
        for proc in procs:
            end_process(proc, retry, timeout_interval)
              ## try best to cleanup processes


@contextmanager
def existing_context(user_settings = None, args = [], retry = 10, retry_first_startup = 2, timeout_interval = 0.3):

    with context(user_settings, args, retry, retry_first_startup, timeout_interval) as ctxt:
        yield ctxt


@contextmanager
def fresh_context(retry = 10, retry_first_startup = 2, timeout_interval = 0.3):

    with tempfile.TemporaryDirectory(prefix = office_name + '.') as user_settings:
        with context(user_settings, ['--headless'], retry, retry_first_startup, timeout_interval) as ctxt:
            yield ctxt


def invoke(args):

    with tempfile.TemporaryDirectory(prefix = office_name + '.') as user_settings:
        call = office_args(args, user_settings = user_settings)
        try:
            subprocess.run(call, check = True)
        except subprocess.CalledProcessError as e:
            if e.returncode != 81:
                raise
        subprocess.run(call, check = True)


def batch_run(*args):

    invoke(['--headless'] + list(args))


def interactive_run(*args):

    invoke(list(args))
